package com.example.myapplication;

/* Lori Postner made a change */
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btn = findViewById(R.id.button);
        Button resetBtn = findViewById(R.id.button_1);
        btn.setOnClickListener(this);
        resetBtn.setOnClickListener(this);
        //
    }

    public void onClick(View view) {
        TextView textView = findViewById(R.id.textView);
        if(view.getId() == R.id.button){
            textView.setText(R.string.bye_msg);
        } else {
            textView.setText(R.string.hello_msg);
        }
    }
}
